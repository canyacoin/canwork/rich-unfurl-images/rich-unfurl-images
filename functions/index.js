const functions = require('firebase-functions');
const firebaseAdmin = require('firebase-admin');

firebaseAdmin.initializeApp();

const userRichImage = require('./src/user').userRichImage;
const jobRichImage = require('./src/job').jobRichImage;
const jobStats = require('./src/jobStats').jobStats;
const publicJobStats = require('./src/jobStats').publicJobStats;

exports.richUnfurlsImages = functions.firestore.document('users/{userId}').onWrite(userRichImage);
exports.richUnfurlsImagesJob = functions.firestore.document('public-jobs/{jobId}').onWrite(jobRichImage);

exports.jobStats = functions.firestore.document('jobs/{jobId}').onUpdate(jobStats);
exports.publicJobStats = functions.firestore.document('public-jobs/{jobId}').onUpdate(publicJobStats);
