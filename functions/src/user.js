const functions = require('firebase-functions');
const firebaseAdmin = require('firebase-admin');
const db = firebaseAdmin.firestore();
const storage = firebaseAdmin.storage();
const bucket = storage.bucket();

const { createCanvas, loadImage } = require('canvas');
const sharp = require('sharp');
const { v4: uuidv4 } = require('uuid');

const avatarParse = require('./util').avatarParse;

const AVATAR_THUMB_WIDTH = 232;

const GOOGLE_STORAGE_BASE_PATH = 'https://firebasestorage.googleapis.com';

/*
How to test into firebase shell:

richUnfurlsImages({before:{}, after:{}}, {params:{userId:'realUserIdFromFirestoreUsers'}});


*/

async function checkAndCompressAvatar(userId, afterData, beforeData) {
  /*
    check if avatar uri is found
    check if avatar uri is on firebase storage
    check if it is changed
    check if compressedAvatarUrl is found
    generate compressed avatar when needed
  */
  
  let avatarChanged = true;

  if (afterData && beforeData) {
    if (afterData.avatar && afterData.avatar.uri) {
      // avatar is found now
      if (beforeData.avatar && (beforeData.avatar.uri === afterData.avatar.uri)) {
        // and it is the same of before
        avatarChanged = false;
      } else {
        // avatar was added, go on
      }
      
    } else {
      // avatar not found now
      if (beforeData.avatar && beforeData.avatar.uri) {
        
        // avatar was removed, we need to remove also compressedAvatarUrl
        // and return
        
        let userUpdate = {
          compressedAvatarUrl: ''
        };
        
        await usersCollection.doc(userId).update(userUpdate);

        functions.logger.info(`userId ${userId} avatar was removed, removed also compressedAvatarUrl`);
        
        return;
        
        
      } else {
        // avatar was not found and it is not found now, nothing to handle
        functions.logger.info(`userId ${userId} avatar not found`);
        return;
        
      }      
      
    }
  }
  
  const usersCollection = db.collection('users');  
  const userRef = usersCollection.doc(userId);
  const userSnapshot = await userRef.get();
  if (!userSnapshot.exists) {
    functions.logger.info(`userId ${userId} not found`);
    return;
  }  
  let user = userSnapshot.data();  
  if (!avatarChanged) {
    // avatar is not changed, let's check if compressedAvatarUrl exists
    /*
    permit forcing generation of compressed avatar setting
    compressedAvatarUrl = 'new'
    */
    if (user.compressedAvatarUrl && (user.compressedAvatarUrl !== 'new')) {
      functions.logger.info(`userId ${userId} avatar not changed, compressed avatar found`);
      return;            
    } else {
      // not changed but compressed not found, we have to generate it same way
      functions.logger.info(`userId ${userId} avatar not changed, but compressed avatar not found`);
    }
  }

  // check if avatar uri is on firebase storage  
  if ( !user.avatar.uri.startsWith(GOOGLE_STORAGE_BASE_PATH)) {
    // it's on external site (like gmail profile image)
    // skip it for now, do not generate compressed avatar and use original one
      functions.logger.info(`userId ${userId} avatar is not on Firebase Storage`);
      if (user.compressedAvatarUrl === 'new') {
        // cleanup, remove field
        await usersCollection.doc(userId).update({
          ["compressedAvatarUrl"]: firebaseAdmin.firestore.FieldValue.delete()          
        });
        
      }
      return;                
  }
  
  functions.logger.info(`generating compressed avatar for userId ${userId}`);
  
  const storageEnvBase = JSON.parse(process.env.FIREBASE_CONFIG).storageBucket;
  
  
  // extract file path from full encoded uri
  let decodedUriSplitted = ( decodeURI(user.avatar.uri) ).split("/");
  let filePath = decodeURIComponent( (decodedUriSplitted[decodedUriSplitted.length - 1]).split("?")[0] );
  
  const splittedFilePath = filePath.split("/");
  const fileName = splittedFilePath[splittedFilePath.length - 1];
  
  const fileBucket = bucket.file(filePath);
  
  const contentType = (await fileBucket.getMetadata() )[0].contentType; // keep same contentType
  
  const downloadResponse = await fileBucket.download();
  const imageBuffer = downloadResponse[0];

  functions.logger.info(`downloaded avatar from ${filePath} for userId ${userId}`);
  
  // Generate a thumbnail using sharp
  const thumbnailBuffer = await sharp(imageBuffer).resize({
    width: AVATAR_THUMB_WIDTH,
    height: null, // auto scale
    withoutEnlargement: true,
  }).toBuffer();
  functions.logger.info(`compressed avatar created for userId ${userId}`);


  const destinationPath = `uploads/avatars/${fileName}_thumb`;
  const destinationFile = bucket.file(destinationPath);
  const destinationUrl = `${GOOGLE_STORAGE_BASE_PATH}/v0/b/${storageEnvBase}/o/${encodeURIComponent(destinationPath)}?alt=media`;  
  const metadata = {
    contentType: contentType
  };
  
  await bucket.file(destinationPath).save(thumbnailBuffer, {
    metadata: metadata,
  });
    
  functions.logger.info(`compressed avatar uploaded to ${destinationPath} for userId ${userId}`);

  const compressedAvatarUrl = destinationUrl
  let userUpdate = {
    compressedAvatarUrl
  };
  
  await usersCollection.doc(userId).update(userUpdate);
  functions.logger.info(`compressed avatar path saved for userId ${userId}`);
  
}



async function generateRichAvatar(userId, afterData, beforeData) {
  
  // We'll only update if richAvatarUrl hasn't changed
  // This is crucial to prevent infinite loops.
  if (
      afterData && 
      beforeData && 
      (afterData.richAvatarUrl !== beforeData.richAvatarUrl) &&
      (afterData.richAvatarUrl !== 'new') // massive update
      ) {
    functions.logger.info(`${userId} richAvatarUrl update event ignored`);
    return null;
  }
  
  const storageEnvBase = JSON.parse(process.env.FIREBASE_CONFIG).storageBucket;

  // 1200 pixels x 627 pixels (1.91/1 ratio)
  const ogImageWidth = 1200;
  const ogImageHeight = 627;
  
  // old, static
  // const storageBasePath = "https://firebasestorage.googleapis.com/v0/b/can-work-io.appspot.com/o/uploads%2Funfurls%2F";
  
  // new, env customized
  const storageBasePath = `${GOOGLE_STORAGE_BASE_PATH}/v0/b/${storageEnvBase}/o/uploads%2Funfurls%2F`;
  
  const backgroundImageUrl = `${storageBasePath}_rich_background.png?alt=media`;
  const verifiedImageUrl = `${storageBasePath}_verified_check.png?alt=media`;
  const fullStarImageUrl = `${storageBasePath}_star_full.png?alt=media`;
  const halfStarImageUrl = `${storageBasePath}_star_half.png?alt=media`;

  const verifiedImage = await loadImage(verifiedImageUrl);
  const fullStarImage = await loadImage(fullStarImageUrl);
  const halfStarImage = await loadImage(halfStarImageUrl);


  

  const usersCollection = db.collection('users');  
  const userRef = usersCollection.doc(userId);
  const userSnapshot = await userRef.get();
  if (!userSnapshot.exists) {
    functions.logger.info(`${userId} not found`);
    return null;
  }  
  let user = userSnapshot.data();


  
  
  let {avatarUrl, avatarInitials} = avatarParse(user.avatar, user.name);


  
  const canvas = createCanvas(ogImageWidth, ogImageHeight);
  
  const ctx = canvas.getContext('2d')



  
  const backgroundImage = await loadImage(backgroundImageUrl);
  ctx.drawImage(backgroundImage, 0, 0, ogImageWidth, ogImageHeight);
  

  let profileName = "Name";
  if (user["name"]) profileName = user["name"];
  
  if (profileName.length > 20) profileName = profileName.substr(0, 20);
  ctx.font = '600 60px Impact, sans-serif'
  ctx.fillStyle = "#323c47";
  ctx.fillText(profileName, 450, 150)

  if (user["verified"]) {
    let textSize = ctx.measureText(profileName);
    ctx.drawImage(verifiedImage, 450+textSize.width+25, 97, 60, 60);
  }

  let profileDescription = "Title";
  if (user["title"]) profileDescription = user["title"];
  
  if (profileDescription.length > 32) profileDescription = profileDescription.substr(0, 32);
  ctx.font = '200 45px Arial, sans-serif'
  ctx.fillStyle = "#424f5d";
  ctx.fillText(profileDescription, 450, 240)
 

  let ratingCalc = 0;
  let ratingCount = 0;      
  let half = false;
  
  if ((user["rating"]) && (user["rating"]["average"])) {
    ratingCalc = Math.floor(user["rating"]["average"]);
    if ((user["rating"]["average"] % 1) > 0) half = true;
  }
  if ((user["rating"]) && (user["rating"]["count"])) {
    ratingCount = user["rating"]["count"];
  }
  
  for (let i=0; i<ratingCalc; i++) { 
    ctx.drawImage(fullStarImage, 450+i*50, 280, 40, 40);  
  }
  if (half) {
    ctx.drawImage(halfStarImage, 450+ratingCalc*50, 280, 40, 40);
    ratingCalc++;
  }
  
  if (ratingCount > 0) {
    let ratingText = `(${ratingCount})`;
    ctx.font = '200 35px Arial, sans-serif'
    ctx.fillStyle = "#a9a9a9";
    ctx.fillText(ratingText, 450+ratingCalc*50+10, 310)    
    
  }
  
  let hourlyRate = "";
  if (user["hourlyRate"]) hourlyRate = user["hourlyRate"];
  
  ctx.font = '700 40px Impact, sans-serif'
  ctx.fillStyle = "#323c47";
  let rateText = "$" + hourlyRate + "/hr"
  ctx.fillText(rateText, 450, 390)


  try {
    // Draw image if exist and correct
    if (avatarUrl.length === 0) throw new Error("Empty url");
    
    const avatarImage = await loadImage(avatarUrl);
    // destination
    let dx = 50;
    let dy = 50;
    let dWidth = 350;
    let dHeight = 350;
    // center image and keep ratio, getting only portion from source with ctx.drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight)
    
    // determine smallest dimension, this will drive choice
    let visiblePortion = 350; // default
    if (avatarImage.width >= avatarImage.height) visiblePortion = avatarImage.height;
      else visiblePortion = avatarImage.width;
      
    // source
    let sWidth = visiblePortion;
    let sHeight = visiblePortion;
    let sx = (avatarImage.width / 2)  - (visiblePortion / 2);
    let sy = (avatarImage.height / 2)  - (visiblePortion / 2);

    ctx.drawImage(avatarImage, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight)  
  } catch(e) {
    // Image wrong, draw circle and initials
    
    ctx.fillStyle = "#33CCFF";
    ctx.beginPath();
    ctx.arc(250, 250, 200, 0, 2 * Math.PI);
    ctx.fill();    
    
    
    ctx.font = '200 150px Arial, sans-serif'
    ctx.fillStyle = "#FFFFFF";
    let initX = 145; // 2 chars
    if (avatarInitials.length === 1) initX = 193; // 1 char
    ctx.fillText(avatarInitials, initX, 298)    
    
  }

  let uuid = uuidv4().replace(/-/g, "");
  let action = 'created';
  if (user.richAvatarUrl) {
    // existing file, overwrite it
    const re = /uploads%2Funfurls%2F([\s|\w]+?).png\?alt=media/i;
    const reRes = re.exec(user.richAvatarUrl);
    if (reRes && (reRes.length === 2)) {
      uuid = reRes[1];
      action = 'updated';
    }
  }
  if (afterData && (afterData.richAvatarUrl === 'new')) action += ' (massive)';

  let randomFileParam = uuidv4().replace(/-/g, ""); // to avoid infinite loop
  
  const destination = `uploads/unfurls/${uuid}.png`;
  const file = bucket.file(destination);
  const richAvatarUrl = `${GOOGLE_STORAGE_BASE_PATH}/v0/b/${storageEnvBase}/o/${encodeURIComponent(destination)}?alt=media&r=${randomFileParam}`;  

  return new Promise((resolve, reject) => {
    canvas.pngStream().pipe(file.createWriteStream({
        metadata: {
          contentType: 'image/png',
          metadata: {
            contentType: 'image/png'
          }
        }
      }))
      .on('error', (err) => {
        functions.logger.info(`${userId} terminated with error`);
        // leave user untouched
        throw(err);
      })
      .on('finish', async () => {
        
        let userUpdate = {
          richAvatarUrl
        };
        
        await usersCollection.doc(userId).update(userUpdate);
        
        functions.logger.info(`${userId} ${action}`);
        resolve();
      });    

  });  
  
}


exports.userRichImage = async function (change, context) {

  let userId = '';
  
  if (context && context.params && context.params.userId) userId = context.params.userId;

  if (!userId) {
    functions.logger.info(`userId not provided`);
    return null;    
  }
  

  const afterData = change.after.data();
  const beforeData = change.before.data();

  try {
    await generateRichAvatar(userId, afterData, beforeData);
    
    functions.logger.info(`generateRichAvatar for userId ${userId} ended`);
    
  } catch (err) {
    functions.logger.info(`generateRichAvatar for userId ${userId} error`, err);
    
  }
  
  try {
    await checkAndCompressAvatar(userId, afterData, beforeData);
    
    functions.logger.info(`checkAndCompressAvatar for userId ${userId} ended`);
    
  } catch (err) {
    functions.logger.info(`checkAndCompressAvatar for userId ${userId} error`, err);
    
  }  
  

  functions.logger.info(`userRichImage for userId ${userId} ended`);
  
  return null;

}
