const functions = require('firebase-functions');
const firebaseAdmin = require('firebase-admin');
const db = firebaseAdmin.firestore();


exports.jobStats = async function (change, context) {
  
  /*
    jobStats({before:{}, after:{}}, {params:{jobId:'realJobIdFromFirestoreJobs'}})  
  */
  let jobId = '';
  
  if (context && context.params && context.params.jobId) jobId = context.params.jobId;

  if (!jobId) {
    functions.logger.info(`jobId not provided`);
    return null;    
  }
  
  const afterData = change.after.data();
  const beforeData = change.before.data();

  // If job state is not changed, stop execution
  if (
      afterData && 
      beforeData && 
      afterData.state &&
      beforeData.state &&
      (afterData.state === beforeData.state) 
      ) return null;
      
  
  // If job state is not 'Complete', stop execution, no new jobs
  if (
      afterData && 
      afterData.state &&
      (afterData.state !== 'Complete') 
      ) return null;
      
  const jobsCollection = db.collection('jobs');

  let stats = {
    name: 'jobs',
    count: 0,
    usd: 0
  }
  
  const jobsTotalSnapshot = await jobsCollection.where('state', '==', 'Complete').get();  
  if (!jobsTotalSnapshot.empty) {
    jobsTotalSnapshot.forEach((doc) => {
      let obj = doc.data();
      stats.count++;
      stats.usd+=Number(obj.budget);
    });
  }
  stats.updatedAt = (new Date()).toISOString();
  
  const statsCollection = db.collection('statistics');
  const statsSnapshot = await statsCollection.where('name', '==', 'jobs').get();
  if (!statsSnapshot.empty) {
    
      // only one
      let statsRef = -1;
      statsSnapshot.forEach((doc) => {
        statsRef = doc.id;
        let obj = doc.data();
        stats.previousUpdatedAt = obj.updatedAt;
        stats.previousCount = obj.count;
        stats.previousUsd = obj.usd;
      });
        
      await statsCollection.doc(statsRef).update(stats);
  } else {
      await statsCollection.add(stats);
  }
  
  return null;    
  
}

exports.publicJobStats = async function (change, context) {
  /*
    publicJobStats({before:{}, after:{}}, {params:{jobId:'realJobIdFromFirestorePublicJobs'}})  
  */
  let jobId = '';
  
  if (context && context.params && context.params.jobId) jobId = context.params.jobId;

  if (!jobId) {
    functions.logger.info(`jobId not provided`);
    return null;    
  }
  
  const afterData = change.after.data();
  const beforeData = change.before.data();

  // If job state is not changed, stop execution
  if (
      afterData && 
      beforeData && 
      afterData.state &&
      beforeData.state &&
      (afterData.state === beforeData.state) 
      ) return null;
      
  
  // If job state is not 'Public job closed', stop execution, no new jobs
  if (
      afterData && 
      afterData.state &&
      (afterData.state !== 'Public job closed') 
      ) return null;
      
  const jobsCollection = db.collection('public-jobs');

  let stats = {
    name: 'publicJobs',
    count: 0,
    usd: 0
  }
  
  const jobsTotalPublicSnapshot = await jobsCollection.where('state', '==', 'Public job closed').get();  
  if (!jobsTotalPublicSnapshot.empty) {
    jobsTotalPublicSnapshot.forEach((doc) => {
      let obj = doc.data();
      stats.count++;
      stats.usd+=Number(obj.budget);
    });
  }
  stats.updatedAt = (new Date()).toISOString();
  
  const statsCollection = db.collection('statistics');
  const statsSnapshot = await statsCollection.where('name', '==', 'publicJobs').get();
  if (!statsSnapshot.empty) {
    
      // only one
      let statsRef = -1;
      statsSnapshot.forEach((doc) => {
        statsRef = doc.id;
        let obj = doc.data();
        stats.previousUpdatedAt = obj.updatedAt;
        stats.previousCount = obj.count;
        stats.previousUsd = obj.usd;
      });
        
      await statsCollection.doc(statsRef).update(stats);
  } else {
      await statsCollection.add(stats);
  }
  
  return null;    

}
