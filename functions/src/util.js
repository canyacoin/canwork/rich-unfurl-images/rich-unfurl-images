
/* from landing */
function avatarParse(avatar, name) {
  let avatarUrl = '';
  let avatarInitials = '?';
  if (avatar && avatar.uri && avatar.uri.trim()) avatarUrl = avatar.uri.trim();
  if (name) avatarInitials = name.split(' ').map(str => str[0]).join('').substring(0, 2).toUpperCase();
  
  return {avatarUrl, avatarInitials}  
}

function wrapText (context, longText, x, y, maxWidth, lineHeight, maxHeight) {
    
    let words = longText.replace(/\n/g, ' ').split(' '),
        line = '',
        lineCount = 0,
        startY = y,
        i,
        test,
        metrics;

    for (i = 0; i < words.length; i++) {
        test = words[i];
        metrics = context.measureText(test);
        while (metrics.width > maxWidth) {
            // Determine how much of the word will fit
            test = test.substring(0, test.length - 1);
            metrics = context.measureText(test);
        }
        if (words[i] !== test) {
            words.splice(i + 1, 0,  words[i].substr(test.length))
            words[i] = test;
        }  

        test = line + words[i] + ' ';  
        metrics = context.measureText(test);
        
        if (metrics.width > maxWidth && i > 0) {
            if (y - startY < maxHeight) context.fillText(line, x, y);
            line = words[i] + ' ';
            y += lineHeight;
            lineCount++;
        }
        else {
            line = test;
        }
    }
    if (y - startY < maxHeight) context.fillText(line, x, y);
}

exports.avatarParse = avatarParse;
exports.wrapText = wrapText;
