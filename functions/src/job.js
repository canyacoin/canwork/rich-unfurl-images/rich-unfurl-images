const functions = require('firebase-functions');
const firebaseAdmin = require('firebase-admin');
const db = firebaseAdmin.firestore();
const storage = firebaseAdmin.storage();
const bucket = storage.bucket();

const { createCanvas, loadImage } = require('canvas');
const { v4: uuidv4 } = require('uuid');

const avatarParse = require('./util').avatarParse;
const wrapText = require('./util').wrapText;


/*
How to test into firebase shell:

richUnfurlsImagesJob({before:{}, after:{}}, {params:{jobId:'realJobIdFromFirestorePublicJobs'}})


*/




exports.jobRichImage = async function (change, context) {
  
  
  let jobId = '';
  
  if (context && context.params && context.params.jobId) jobId = context.params.jobId;

  if (!jobId) {
    functions.logger.info(`jobId not provided`);
    return null;    
  }
  
  const afterData = change.after.data();
  const beforeData = change.before.data();

  // We'll only update if richAvatarUrl hasn't changed
  // This is crucial to prevent infinite loops.
  if (
      afterData && 
      beforeData && 
      (afterData.richAvatarUrl !== beforeData.richAvatarUrl)
      ) {
    functions.logger.info(`${jobId} richAvatarUrl update event ignored`);
    return null;
  }
  
  // Ignore also updateAt field update from 'updatepublicJobTimeStamp' Firebase Function
  // To avoid double updates
  if (
      afterData && 
      beforeData && 
      (afterData.updateAt !== beforeData.updateAt)
      ) {
    functions.logger.info(`${jobId} updateAt update event ignored`);
    return null;
  }  
  
  
  const storageEnvBase = JSON.parse(process.env.FIREBASE_CONFIG).storageBucket;
  
  
  // 1200 pixels x 627 pixels (1.91/1 ratio)
  const ogImageWidth = 1200;
  const ogImageHeight = 627;
  
  // new, env customized  
  const storageBasePath = `https://firebasestorage.googleapis.com/v0/b/${storageEnvBase}/o/uploads%2Funfurls%2F`;

  const backgroundImageUrl = `${storageBasePath}_rich_background.png?alt=media`;
  const verifiedImageUrl = `${storageBasePath}_verified_check.png?alt=media`;
  const fullStarImageUrl = `${storageBasePath}_star_full.png?alt=media`;
  const halfStarImageUrl = `${storageBasePath}_star_half.png?alt=media`;

  const verifiedImage = await loadImage(verifiedImageUrl);
  const fullStarImage = await loadImage(fullStarImageUrl);
  const halfStarImage = await loadImage(halfStarImageUrl);



  const jobsCollection = db.collection('public-jobs');  
  const jobRef = jobsCollection.doc(jobId);
  const jobSnapshot = await jobRef.get();
  if (!jobSnapshot.exists) {
    functions.logger.info(`${jobId} job not found`);
    return null;
  }  
  let job = jobSnapshot.data();
  
  if (!job.clientId) {
    functions.logger.info(`${jobId} without clientId`);
    return null;
  }   
  
  let userId = job.clientId;
  
  const usersCollection = db.collection('users');  
  const userRef = usersCollection.doc(userId);
  const userSnapshot = await userRef.get();
  if (!userSnapshot.exists) {
    functions.logger.info(`job ${jobId}: ${userId} user not found`);
    return null;
  }  
  let user = userSnapshot.data();

  let {avatarUrl, avatarInitials} = avatarParse(user.avatar, user.name);

  //functions.logger.info(job.slug, user.slug);
  
  const canvas = createCanvas(ogImageWidth, ogImageHeight);
  
  const ctx = canvas.getContext('2d')



  
  const backgroundImage = await loadImage(backgroundImageUrl);
  ctx.drawImage(backgroundImage, 0, 0, ogImageWidth, ogImageHeight);  
  
  let jobTitle = "Title";
  if (job.information && job.information.title) jobTitle = job.information.title;
  if (jobTitle.length > 33) jobTitle = jobTitle.substr(0, 33);
  ctx.font = '500 60px Impact, sans-serif'
  ctx.fillStyle = "#323c47";
  ctx.fillText(jobTitle, 50, 90)
  
  let jobDescription = "Description";
  if (job.information && job.information.description) jobDescription = job.information.description;
  if (jobDescription.length > 200) jobDescription = jobDescription.substr(0, 200);  
  ctx.font = '200 40px Arial, sans-serif'
  ctx.fillStyle = "#424f5d"; 
  wrapText(ctx, jobDescription, 60, 155, 1080, 55, 200);
  
  
  // destination
  let dx = 60;
  let dy = 342;
  let dWidth = 140;
  let dHeight = 140;  
  try {
    // Draw image if exist and correct
    if (avatarUrl.length === 0) throw new Error("Empty url"); // draw initials
    const avatarImage = await loadImage(avatarUrl);

    // center image and keep ratio, getting only portion from source with ctx.drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight)
    
    // determine smallest dimension, this will drive choice
    let visiblePortion = 140; // default
    if (avatarImage.width >= avatarImage.height) visiblePortion = avatarImage.height;
      else visiblePortion = avatarImage.width;
      
    // source
    let sWidth = visiblePortion;
    let sHeight = visiblePortion;
    let sx = (avatarImage.width / 2)  - (visiblePortion / 2);
    let sy = (avatarImage.height / 2)  - (visiblePortion / 2);

    ctx.drawImage(avatarImage, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight)  
  } catch(e) {
    // Image wrong, draw circle and initials
    ctx.fillStyle = "#33CCFF";
    ctx.beginPath();
    ctx.arc(dx+dWidth/2, dy+dHeight/2, dWidth/2, 0, 2 * Math.PI);
    ctx.fill();    

    ctx.font = '200 60px Arial, sans-serif'
    ctx.fillStyle = "#FFFFFF";
    let initX = 90; // 2 chars
    if (avatarInitials.length === 1) initX = 110; // 1 char
    ctx.fillText(avatarInitials, initX, 432)    
    
  }
  
  let profileName = "Name";
  if (user["name"]) profileName = user["name"];
  
  if (profileName.length > 15) profileName = profileName.substr(0, 15);
  ctx.font = '400 50px Impact, sans-serif'
  ctx.fillStyle = "#323c47";
  ctx.fillText(profileName, 230, 420)

  if (user["verified"]) {
    let textSize = ctx.measureText(profileName);
    ctx.drawImage(verifiedImage, 230+textSize.width+15, 382, 40, 40);
  }  
  
  let budget = "..";
  if (job["budget"]) budget = job["budget"];
  let jobBudget = `$${budget} USD`;
  
  ctx.font = '400 50px Impact, sans-serif'
  ctx.fillStyle = "#323c47";
  let bdgSize = ctx.measureText(jobBudget);
  ctx.fillText(jobBudget, 1130-bdgSize.width, 420);
  
  
  let uuid = 'j_'+ uuidv4().replace(/-/g, ""); // j prefix (regex doesn't need to be changed)
  let action = 'created';
  if (job.richAvatarUrl) {
    // existing file, overwrite it
    const re = /uploads%2Funfurls%2F([\s|\w]+?).png\?alt=media/i;
    const reRes = re.exec(job.richAvatarUrl);
    if (reRes && (reRes.length === 2)) {
      uuid = reRes[1];
      action = 'updated';
    }
  }

  let randomFileParam = uuidv4().replace(/-/g, ""); // to avoid infinite loop
  
  const destination = `uploads/unfurls/${uuid}.png`;
  const file = bucket.file(destination);
  const richAvatarUrl = `https://firebasestorage.googleapis.com/v0/b/${storageEnvBase}/o/${encodeURIComponent(destination)}?alt=media&r=${randomFileParam}`;  

  return new Promise((resolve, reject) => {
    canvas.pngStream().pipe(file.createWriteStream({
        metadata: {
          contentType: 'image/png',
          metadata: {
            contentType: 'image/png'
          }
        }
      }))
      .on('error', (err) => {
        functions.logger.info(`${jobId} terminated with error`);
        // leave job untouched
        throw(err);
      })
      .on('finish', async () => {
        
        let jobUpdate = {
          richAvatarUrl
        };
        
        await jobsCollection.doc(jobId).update(jobUpdate);
        
        functions.logger.info(`${jobId} ${action}`);
        resolve();
      });    

  });
}
